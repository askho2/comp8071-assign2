﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Comp8071_Assign2.Models
{
    [Table("CustomerServiceSchedule")]
    public partial class CustomerServiceSchedule
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("CustomerID")]
        public int CustomerId { get; set; }
        [Column("ServiceTypeID")]
        public int ServiceTypeId { get; set; }
        [Column("EmployeeID")]
        public int EmployeeId { get; set; }
        public DateTime? StartDatetime { get; set; }
        public int? ActualDuration { get; set; }
        [StringLength(500)]
        public string Status { get; set; }

        [ForeignKey(nameof(CustomerId))]
        [InverseProperty("CustomerServiceSchedules")]
        public virtual Customer Customer { get; set; }
        [ForeignKey(nameof(EmployeeId))]
        [InverseProperty("CustomerServiceSchedules")]
        public virtual Employee Employee { get; set; }
        [ForeignKey(nameof(ServiceTypeId))]
        [InverseProperty("CustomerServiceSchedules")]
        public virtual ServiceType ServiceType { get; set; }
    }
}
