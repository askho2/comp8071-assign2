﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Comp8071_Assign2.Models
{
    [Table("ServiceType")]
    public partial class ServiceType
    {
        public ServiceType()
        {
            CustomerServiceSchedules = new HashSet<CustomerServiceSchedule>();
            CustomerServices = new HashSet<CustomerService>();
        }

        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [StringLength(500)]
        public string Name { get; set; }
        [StringLength(500)]
        public string CertificationRqts { get; set; }
        [Column(TypeName = "money")]
        public decimal? Rate { get; set; }

        [InverseProperty(nameof(CustomerServiceSchedule.ServiceType))]
        public virtual ICollection<CustomerServiceSchedule> CustomerServiceSchedules { get; set; }
        [InverseProperty(nameof(CustomerService.ServiceType))]
        public virtual ICollection<CustomerService> CustomerServices { get; set; }
    }
}
