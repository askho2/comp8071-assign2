﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Comp8071_Assign2.Models
{
    [Table("Customer")]
    public partial class Customer
    {
        public Customer()
        {
            CustomerServiceSchedules = new HashSet<CustomerServiceSchedule>();
            CustomerServices = new HashSet<CustomerService>();
        }

        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [StringLength(500)]
        public string Name { get; set; }
        [StringLength(500)]
        public string Street { get; set; }
        [StringLength(500)]
        public string City { get; set; }
        [Column(TypeName = "date")]
        public DateTime? Birthdate { get; set; }
        [Column(TypeName = "image")]
        public byte[] Picture { get; set; }
        [StringLength(1)]
        public string Gender { get; set; }

        [InverseProperty(nameof(CustomerServiceSchedule.Customer))]
        public virtual ICollection<CustomerServiceSchedule> CustomerServiceSchedules { get; set; }
        [InverseProperty(nameof(CustomerService.Customer))]
        public virtual ICollection<CustomerService> CustomerServices { get; set; }
    }
}
