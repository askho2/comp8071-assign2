﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Comp8071_Assign2.Models
{
    [Table("Employee")]
    public partial class Employee
    {
        public Employee()
        {
            CustomerServiceSchedules = new HashSet<CustomerServiceSchedule>();
            InverseManager = new HashSet<Employee>();
        }

        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [StringLength(500)]
        public string Certification { get; set; }
        [StringLength(500)]
        public string City { get; set; }
        [StringLength(500)]
        public string JobTitle { get; set; }
        [Column("ManagerID")]
        public int? ManagerId { get; set; }
        [StringLength(500)]
        public string Name { get; set; }
        [StringLength(500)]
        public string Street { get; set; }
        [Column(TypeName = "money")]
        public decimal? Salary { get; set; }

        [ForeignKey(nameof(ManagerId))]
        [InverseProperty(nameof(Employee.InverseManager))]
        public virtual Employee Manager { get; set; }
        [InverseProperty(nameof(CustomerServiceSchedule.Employee))]
        public virtual ICollection<CustomerServiceSchedule> CustomerServiceSchedules { get; set; }
        [InverseProperty(nameof(Employee.Manager))]
        public virtual ICollection<Employee> InverseManager { get; set; }
    }
}
