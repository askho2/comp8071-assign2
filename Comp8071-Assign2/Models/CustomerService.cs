﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Comp8071_Assign2.Models
{
    [Table("CustomerService")]
    public partial class CustomerService
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("CustomerID")]
        public int CustomerId { get; set; }
        [Column("ServiceTypeID")]
        public int ServiceTypeId { get; set; }
        public int? ExpectedDuration { get; set; }

        [ForeignKey(nameof(CustomerId))]
        [InverseProperty("CustomerServices")]
        public virtual Customer Customer { get; set; }
        [ForeignKey(nameof(ServiceTypeId))]
        [InverseProperty("CustomerServices")]
        public virtual ServiceType ServiceType { get; set; }
    }
}
