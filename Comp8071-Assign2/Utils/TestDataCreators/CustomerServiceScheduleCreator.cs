﻿using System;
using System.Collections.Generic;
using System.Text;
using Comp8071_Assign2.Models;

namespace Comp8071_Assign2.Utils.TestDataCreators
{
    static class CustomerServiceScheduleCreator
    {
        public static string[] STATUSES = { "OPEN", "IN_PROGRESS", "COMPLETED", "CANCELLED" };
        public static CustomerServiceSchedule createCustomerServiceSchedule(Customer customer, 
            ServiceType serviceType, Employee employee)
        {
            CustomerServiceSchedule css = new CustomerServiceSchedule();
            css.CustomerId = customer.Id;
            css.ServiceTypeId = serviceType.Id;
            css.EmployeeId = employee.Id;
            css.StartDatetime = GeneralUtils.getRandomDateTimeBetween(DateTime.Now, DateTime.Now.AddYears(1));
            css.ActualDuration = Faker.RandomNumber.Next(1, 20000);
            css.Status = GeneralUtils.fetchRandomArrayElement(STATUSES);
            return css;
        }
    }
}
