﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Comp8071_Assign2.Models;

namespace Comp8071_Assign2.Utils.TestDataCreators
{
    class CustomerCreator
    {
        private readonly static string[] IMAGE_NAMES = { 
            "image1.jpg",
            "image2.jpg",
            "image3.jpg",
            "image4.jpg",
            "image5.jpg",
            "image6.jpg",
            "image7.jpg",
            "image8.jpg",
            "image9.jpg",
            "image10.jpg"
        };

        private readonly static string[] POSSIBLE_GENDERS =
        {
            "M", "F"
        };

        public static Customer createCustomer()
        {
            Customer testCustomer = new Customer();
            testCustomer.Name = Faker.Name.FullName();
            testCustomer.Birthdate = Faker.Identification.DateOfBirth();
            testCustomer.Street = Faker.Address.StreetAddress();
            testCustomer.City = Faker.Address.City();
            testCustomer.Gender = GeneralUtils.fetchRandomArrayElement(POSSIBLE_GENDERS);
            testCustomer.Picture = loadRandomImage();
            return testCustomer;
        }

        private static byte[] loadRandomImage()
        {
            string imageName = GeneralUtils.fetchRandomArrayElement(IMAGE_NAMES);
            Assembly assembly = Assembly.GetEntryAssembly();
            using (Stream resourceStream = assembly.GetManifestResourceStream($"Comp8071_Assign2.Resources.images.{imageName}"))            {
                if (resourceStream == null) return null;
                byte[] ba = new byte[resourceStream.Length];
                resourceStream.Read(ba, 0, ba.Length);
                return ba;
            }
        }
    }

}
