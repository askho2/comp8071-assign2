﻿using System;
using System.Collections.Generic;
using System.Text;
using Comp8071_Assign2.Models;

namespace Comp8071_Assign2.Utils.TestDataCreators
{
    static class ServiceTypeCreator
    {
        private static readonly string[] SERVICE_NAME = {
            "new website",
            "new android app",
            "new iOS app",
            "new hybrid app",
            "backend server development",
            "cloud migration",
            "CI CD consultation",
            "general consultation",
            "ERP software migration",
            "POS development",
            "mac app development"
        };

        public static ServiceType createServiceType()
        {
            ServiceType serviceType = new ServiceType();
            serviceType.Rate = Faker.RandomNumber.Next(10, 500);
            serviceType.CertificationRqts = GeneralUtils.fetchRandomArrayElement(EmployeeCreator.CERTIFICATIONS);
            serviceType.Name = GeneralUtils.fetchRandomArrayElement(SERVICE_NAME);

            return serviceType;

        }
    }
}
