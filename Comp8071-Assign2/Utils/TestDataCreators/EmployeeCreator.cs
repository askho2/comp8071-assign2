﻿using System;
using System.Collections.Generic;
using System.Text;
using Comp8071_Assign2.Models;

namespace Comp8071_Assign2.Utils.TestDataCreators
{
    public static class EmployeeCreator
    {
        public static readonly string[] CERTIFICATIONS = {
            "GCP",
            "AWS",
            "CISM",
            "CRISC",
            "CISSP",
            "CISA",
            "PMP",
            "CCA-N",
            "CCP-V",
            "CPR",
            "PHR",
            "Comp TIA A+",
            "ACA"
        };

        public static readonly string[] JOB_TITLES =
        {
            "Junior Software Engineer",
            "Senior Software Engineer",
            "Staff Software Engineer",
            "Intern Software Engineer",
            "Intern business analyst",
            "Technical Business Analyst",
            "Product Developer",
            "Project Manager",
            "Senior Project Manager",
            "Associate Project Manager",
            "Tech lead",
            "Architecture lead",
            "Enterprise architect"
        };

        public static Employee createEmployee()
        {
            Employee employee = new Employee();
            employee.Name = Faker.Name.FullName();
            employee.City = Faker.Address.City();
            employee.Certification = GeneralUtils.fetchRandomArrayElement(CERTIFICATIONS);
            employee.JobTitle = GeneralUtils.fetchRandomArrayElement(JOB_TITLES);
            employee.Street = Faker.Address.StreetAddress();
            employee.Salary = Faker.RandomNumber.Next(10000, 500000);
            return employee;
        }

        public static Employee createEmployeeWithManager()
        {
            Employee employee = createEmployee();
            employee.Manager = createEmployee();
            return employee;
        }
    }
}
