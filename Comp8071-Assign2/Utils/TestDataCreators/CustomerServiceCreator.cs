﻿using System;
using System.Collections.Generic;
using System.Text;
using Comp8071_Assign2.Models;

namespace Comp8071_Assign2.Utils.TestDataCreators
{
    static class CustomerServiceCreator
    {
        public static CustomerService createCustomerService(Customer customer, ServiceType serviceType)
        {
            CustomerService cs = new CustomerService();
            cs.Customer = customer;
            cs.ServiceType = serviceType;
            cs.ExpectedDuration = Faker.RandomNumber.Next(1, 10000);
            return cs;
        }
    }
}
