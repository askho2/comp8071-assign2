﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Comp8071_Assign2.Utils.TestDataCreators
{
    static class GeneralUtils
    {
       public static T fetchRandomArrayElement<T>(T[] input)
        {
            Random random = new Random();
            return input[random.Next(0, input.Length)];
        }

        public static DateTime getRandomDateTimeBetween(DateTime startdate, DateTime endDate)
        {
            Random random = new Random();
            TimeSpan timeSpan = endDate - startdate;
            TimeSpan newSpan = new TimeSpan(0, random.Next(0, (int)timeSpan.TotalMinutes), 0);
            return startdate + newSpan;
        }
    }
}
