﻿using Comp8071_Assign2.Models;
using System;
using Comp8071_Assign2.Utils.TestDataCreators;
using System.Transactions;

namespace Comp8071_Assign2
{
    class Program
    {
        static readonly int SCHEDULES_TO_CREATE = 50000;
        static readonly int CUSTOMERS_TO_CREATE = 5000;
        static readonly int EMPLOYEES_TO_CREATE = 2000;
        static readonly int SERVICE_TYPES_TO_CREATE = 500;
        static readonly int CUSTOMER_SERVICES_TO_CREATE = 1000;
        static void Main(string[] args)
        {
            Customer[] customers = createCustomers();
            Employee[] employees = createEmployees();
            ServiceType[] serviceTypes = createServiceTypes();
            CustomerService[] customerServices = createCustomerServices(customers, serviceTypes);
            

            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted}))
            {
                using COMP8071Context context = new COMP8071Context();
                context.Customers.AddRange(customers);
                context.Employees.AddRange(employees);
                context.ServiceTypes.AddRange(serviceTypes);
                context.CustomerServices.AddRange(customerServices);
                context.SaveChanges();
                scope.Complete();
            }
            CustomerServiceSchedule[] customerServiceSchedules = createCustomerServiceSchedules(customerServices, employees);
            saveCustomerServiceSchedules(customerServiceSchedules);

        }

        private static void saveCustomerServiceSchedules(CustomerServiceSchedule[] customerServiceSchedules)
        {
            foreach (CustomerServiceSchedule cs in customerServiceSchedules) {
                using COMP8071Context context = new COMP8071Context();
                context.AddRange(cs);
                context.SaveChanges();
            }
        }

        private static Customer[] createCustomers()
        {
            Customer[] customerArray = new Customer[CUSTOMERS_TO_CREATE]; 
            for (int i = 0; i < CUSTOMERS_TO_CREATE; i++)
            {
                customerArray[i] = CustomerCreator.createCustomer();
            }
            return customerArray;
        }

        private static Employee[] createEmployees()
        {
            Employee[] employeeArray = new Employee[EMPLOYEES_TO_CREATE];
            for (int i = 0; i < EMPLOYEES_TO_CREATE; i++)
            {
                employeeArray[i] = EmployeeCreator.createEmployee();
            }
            return employeeArray;
        }

        private static ServiceType[] createServiceTypes()
        {
            ServiceType[] serviceTypes = new ServiceType[SERVICE_TYPES_TO_CREATE];
            for (int i = 0; i < SERVICE_TYPES_TO_CREATE; i++)
            {
                serviceTypes[i] = ServiceTypeCreator.createServiceType();
            }
            return serviceTypes;
        }

        private static CustomerService[] createCustomerServices(Customer[] customers, ServiceType[] servicesTypes)
        {
            CustomerService[] customerServices = new CustomerService[CUSTOMER_SERVICES_TO_CREATE];
            for ( int i = 0; i < CUSTOMER_SERVICES_TO_CREATE; i++)
            {
                customerServices[i] = CustomerServiceCreator.createCustomerService(
                    GeneralUtils.fetchRandomArrayElement(customers), 
                    GeneralUtils.fetchRandomArrayElement(servicesTypes));
            }
            return customerServices;
        }

        private static CustomerServiceSchedule[] createCustomerServiceSchedules(CustomerService[] customerServices, Employee[] employees)
        {
            CustomerServiceSchedule[] schedules = new CustomerServiceSchedule[SCHEDULES_TO_CREATE];
            for ( int i = 0; i < SCHEDULES_TO_CREATE; i ++)
            {
                CustomerService cs = GeneralUtils.fetchRandomArrayElement(customerServices);
                Employee employee = GeneralUtils.fetchRandomArrayElement(employees);
                cs.ServiceTypeId = cs.ServiceType.Id;

                schedules[i] = CustomerServiceScheduleCreator.createCustomerServiceSchedule(
                        cs.Customer,
                        cs.ServiceType,
                        employee
                    );
            }

            return schedules;
        }
    }
}
