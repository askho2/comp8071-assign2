﻿CREATE TABLE [dbo].[service_dim] (
    [service_id] INT           NOT NULL,
    [name]       VARCHAR (500) NULL,
    CONSTRAINT [service_id_pk] PRIMARY KEY CLUSTERED ([service_id] ASC)
);

