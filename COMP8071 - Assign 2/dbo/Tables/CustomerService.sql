﻿CREATE TABLE [dbo].[CustomerService] (
    [ID]               INT IDENTITY (1, 1) NOT NULL,
    [CustomerID]       INT NOT NULL,
    [ServiceTypeID]    INT NOT NULL,
    [ExpectedDuration] INT NULL,
    CONSTRAINT [customer_service_pkey] PRIMARY KEY CLUSTERED ([ID] ASC),
    FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customer] ([ID]),
    FOREIGN KEY ([ServiceTypeID]) REFERENCES [dbo].[ServiceType] ([ID])
);

