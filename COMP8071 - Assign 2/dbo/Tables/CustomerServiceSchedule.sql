﻿CREATE TABLE [dbo].[CustomerServiceSchedule] (
    [ID]             INT           IDENTITY (1, 1) NOT NULL,
    [CustomerID]     INT           NOT NULL,
    [ServiceTypeID]  INT           NOT NULL,
    [EmployeeID]     INT           NOT NULL,
    [StartDatetime]  DATETIME2 (7) NULL,
    [ActualDuration] INT           NULL,
    [Status]         VARCHAR (500) NULL,
    CONSTRAINT [customer_service_schedule_pkey] PRIMARY KEY CLUSTERED ([ID] ASC),
    FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customer] ([ID]),
    FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employee] ([ID]),
    FOREIGN KEY ([ServiceTypeID]) REFERENCES [dbo].[ServiceType] ([ID])
);


GO
CREATE TRIGGER serviceScheduleCreate ON CustomerServiceSchedule
AFTER INSERT
AS 
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    IF NOT EXISTS ( SELECT 1 
                    FROM time_dim as td
                    JOIN inserted ON td.year = YEAR(inserted.StartDatetime)
                        AND td.month = MONTH(inserted.StartDatetime)
                        AND td.week = datepart(week, inserted.StartDatetime)
                        AND td.day = DAY(inserted.StartDatetime)
    )
    BEGIN

    INSERT INTO time_dim (date_time, year, month, day, week)
        SELECT inserted.StartDatetime, YEAR(inserted.StartDatetime), MONTH(inserted.StartDatetime), DAY(inserted.StartDatetime), datepart(week, inserted.StartDatetime)
        FROM inserted;
    END

    INSERT INTO sales_fact (customer_id, service_id, time_id, employee_id, manager_name, expected_duration, actual_duration, income, revenue)
    SELECT i.CustomerID, i.ServiceTypeID, td.time_id, i.EmployeeID, m.name, cs.ExpectedDuration, i.ActualDuration, i.ActualDuration * st.Rate, (i.ActualDuration * st.Rate) - ((e.Salary / 2000) * i.ActualDuration)
    FROM inserted i
        LEFT JOIN CustomerService cs ON cs.ServiceTypeID = i.ServiceTypeID AND cs.CustomerID = i.CustomerID
        LEFT JOIN ServiceType st on st.ID = i.ServiceTypeID
        LEFT JOIN Employee e ON e.ID = i.EmployeeID
        LEFT JOIN Employee m ON m.ID = e.ManagerID
        LEFT JOIN time_dim td ON td.year = YEAR(i.StartDatetime)
                        AND td.month = MONTH(i.StartDatetime)
                        AND td.week = datepart(week, i.StartDatetime)
                        AND td.day = DAY(i.StartDatetime)