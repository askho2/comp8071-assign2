﻿CREATE TABLE [dbo].[ServiceType] (
    [ID]                INT           IDENTITY (1, 1) NOT NULL,
    [Name]              VARCHAR (500) NULL,
    [CertificationRqts] VARCHAR (500) NULL,
    [Rate]              MONEY         NULL,
    CONSTRAINT [ServiceType_pkey] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE TRIGGER createServiceTypes ON ServiceType
AFTER INSERT
AS
    IF EXISTS (SELECT * FROM DELETED)
    BEGIN
        UPDATE service_dim SET 
            service_dim.service_id = inserted.ID,
            service_dim.name = inserted.name
        FROM service_dim JOIN inserted ON service_dim.service_id= inserted.ID
    END
    ELSE
    BEGIN
        INSERT INTO service_dim(service_id, name)
            SELECT inserted.ID, inserted.name
            FROM inserted
    END
