﻿CREATE TABLE [dbo].[Employee] (
    [ID]            INT           IDENTITY (1, 1) NOT NULL,
    [Certification] VARCHAR (500) NULL,
    [City]          VARCHAR (500) NULL,
    [JobTitle]      VARCHAR (500) NULL,
    [ManagerID]     INT           NULL,
    [Name]          VARCHAR (500) NULL,
    [Street]        VARCHAR (500) NULL,
    [Salary]        MONEY         NULL,
    CONSTRAINT [Employee_pkey] PRIMARY KEY CLUSTERED ([ID] ASC),
    FOREIGN KEY ([ManagerID]) REFERENCES [dbo].[Employee] ([ID])
);


GO
CREATE TRIGGER createEmployee ON Employee
AFTER INSERT
AS
    IF EXISTS (SELECT * FROM DELETED)
    BEGIN
        UPDATE employee_dim SET 
            employee_dim.name = inserted.name, 
            employee_dim.salary = inserted.salary,
            employee_dim.jobtitle = inserted.jobtitle
        FROM employee_dim JOIN inserted ON employee_dim.employee_id = inserted.ID
    END
    ELSE
    BEGIN
        INSERT INTO employee_dim(employee_id, name, salary, jobtitle)
            SELECT inserted.ID, inserted.name, inserted.Salary, inserted.JobTitle
            FROM inserted
    END
