﻿CREATE TABLE [dbo].[customer_dim] (
    [customer_id] INT           NOT NULL,
    [name]        VARCHAR (500) NULL,
    [gender]      CHAR (1)      NULL,
    CONSTRAINT [customer_id_pk] PRIMARY KEY CLUSTERED ([customer_id] ASC)
);

