﻿CREATE TABLE [dbo].[Customer] (
    [ID]        INT           IDENTITY (1, 1) NOT NULL,
    [Name]      VARCHAR (500) NULL,
    [Street]    VARCHAR (500) NULL,
    [City]      VARCHAR (500) NULL,
    [Birthdate] DATE          NULL,
    [Picture]   IMAGE         NULL,
    [Gender]    CHAR (1)      NULL,
    CONSTRAINT [customer_pkey] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE TRIGGER customerCreate ON Customer
AFTER INSERT
AS
    IF EXISTS (SELECT * FROM DELETED)
    BEGIN
        UPDATE customer_dim SET customer_dim.name = inserted.name, customer_dim.gender = inserted.gender
        FROM customer_dim JOIN inserted ON customer_dim.customer_id = inserted.ID
    END
    ELSE
    BEGIN
        INSERT INTO customer_dim (customer_id, name, gender)
            SELECT inserted.ID, inserted.name, inserted.gender
            FROM inserted
    END