﻿CREATE TABLE [dbo].[sales_fact] (
    [customer_id]       INT           NOT NULL,
    [service_id]        INT           NOT NULL,
    [time_id]           INT           NOT NULL,
    [employee_id]       INT           NOT NULL,
    [manager_name]      VARCHAR (500) NULL,
    [expected_duration] INT           NULL,
    [actual_duration]   INT           NULL,
    [income]            INT           NULL,
    [revenue]           INT           NULL,
    FOREIGN KEY ([customer_id]) REFERENCES [dbo].[customer_dim] ([customer_id]),
    FOREIGN KEY ([employee_id]) REFERENCES [dbo].[employee_dim] ([employee_id]),
    FOREIGN KEY ([service_id]) REFERENCES [dbo].[service_dim] ([service_id]),
    FOREIGN KEY ([time_id]) REFERENCES [dbo].[time_dim] ([time_id])
);

