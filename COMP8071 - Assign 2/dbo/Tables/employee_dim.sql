﻿CREATE TABLE [dbo].[employee_dim] (
    [employee_id] INT           NOT NULL,
    [name]        VARCHAR (500) NULL,
    [salary]      MONEY         NULL,
    [jobtitle]    VARCHAR (500) NULL,
    CONSTRAINT [employee_id_pk] PRIMARY KEY CLUSTERED ([employee_id] ASC)
);

