﻿CREATE TABLE [dbo].[time_dim] (
    [time_id]   INT           IDENTITY (1, 1) NOT NULL,
    [date_time] DATETIME2 (7) NULL,
    [year]      NUMERIC (4)   NULL,
    [month]     NUMERIC (2)   NULL,
    [day]       NUMERIC (2)   NULL,
    [week]      NUMERIC (2)   NULL,
    CONSTRAINT [time_dim_pk] PRIMARY KEY CLUSTERED ([time_id] ASC),
    CONSTRAINT [uq_date] UNIQUE NONCLUSTERED ([year] ASC, [month] ASC, [day] ASC, [week] ASC)
);

