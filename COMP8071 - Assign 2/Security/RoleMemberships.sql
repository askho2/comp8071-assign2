﻿ALTER ROLE [db_owner] ADD MEMBER [leon];


GO
ALTER ROLE [db_accessadmin] ADD MEMBER [leon];


GO
ALTER ROLE [db_securityadmin] ADD MEMBER [leon];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [leon];


GO
ALTER ROLE [db_backupoperator] ADD MEMBER [leon];


GO
ALTER ROLE [db_datareader] ADD MEMBER [leon];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [leon];


GO
ALTER ROLE [db_denydatareader] ADD MEMBER [leon];


GO
ALTER ROLE [db_denydatawriter] ADD MEMBER [leon];

